//Import Human class
const {Human} = require('./humanClass'); 

const Worker = class extends Human {
  constructor(fullName, DOB, homeTown, job, workPlace, salary) {
    super(fullName, DOB, homeTown);
    this.job = job;
    this.workPlace = workPlace;
    this.salary = salary;
  }
  getInfo() {
    console.log("In ra info của công nhân");
    var info = {
      fullName: this.fullName, 
      DOB: this.DOB, 
      homeTown: this.homeTown, 
      job: this.job, 
      workPlace: this.workPlace, 
      salary: this.salary
    }
    return info;
  }
}

let obj = new Worker("Quan", "24/03/2000", "SG", "coder", "NVC", 10000000);
console.log(obj.getInfo());