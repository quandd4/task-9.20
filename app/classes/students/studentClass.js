//Import Human class
const {Human} = require('../humanClass');

const Student = class extends Human {
  constructor(fullName, DOB, homeTown, schoolName, grade, phoneNumber) {
    super(fullName, DOB, homeTown);
    this.schoolName = schoolName;
    this.grade = grade;
    this.phoneNumber = phoneNumber
  }
}

module.exports = {Student}; //Export class Human để xài cho các class khác