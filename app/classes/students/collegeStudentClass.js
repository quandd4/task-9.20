//Import Student class
const {Student} = require('./studentClass');

//Import Human class
const {Human} = require('../humanClass'); 

const collegeStudent = class extends Student {
  constructor(fullName, DOB, homeTown, schoolName, grade, phoneNumber, major, studentCode){
    super(fullName, DOB, homeTown, schoolName, grade, phoneNumber);
    this.major = major;
    this.studentCode = studentCode;
  }
  getInfo() {
    console.log("In ra info của sinh viên");
    var info = {
      fullName: this.fullName, 
      DOB: this.DOB, 
      homeTown: this.homeTown, 
      schoolName: this.schoolName,
      grade: this.grade,
      phoneNumber: this.phoneNumber,
      major: this.major,
      studentCode: this.studentCode
    }
    return info;
  }
}

let obj = new collegeStudent("Quan", 2000, "TPHCM", "IronHack", "R2223", "0123456789", "fullstack", "2403");
console.log(obj.getInfo());
console.log(obj instanceof Human);
console.log(obj instanceof Student);